package lt.codeacademy.dbexam.entities;

import java.io.Serializable;

public class ManageRightsPK implements Serializable {
	
	protected String login;
	protected Integer questionnaireId;
	
	public ManageRightsPK() {
	}
	
	public ManageRightsPK(String login, Integer questionnaireId) {
		this.login = login;
		this.questionnaireId = questionnaireId;
	}
	
	public String getLogin() {
		return login;
	}
	
	public void setLogin(String login) {
		this.login = login;
	}
	
	public Integer getQuestionnaireId() {
		return questionnaireId;
	}
	
	public void setQuestionnaireId(Integer questionnaireId) {
		this.questionnaireId = questionnaireId;
	}
	
	@Override
	public String toString() {
		return "ManageRightsId{" +
				"login='" + login + '\'' +
				", questionnaireId=" + questionnaireId +
				'}';
	}
}
