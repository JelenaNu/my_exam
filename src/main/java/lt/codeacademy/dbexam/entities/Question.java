package lt.codeacademy.dbexam.entities;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "question")
public class Question {
	
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name = "questionnaire_id")
	private Integer questionnaireId;
	
	@Column(name = "text")
	private String text;

	@OneToMany (mappedBy = "questionId", cascade = CascadeType.ALL)
	private List<Option> options = new ArrayList<>();
	
	public Question() {
	}
	
	public Question(Integer questionnaireId, String text) {
		this.questionnaireId = questionnaireId;
		this.text = text;
	}
	
	public List<Option> getOptions() {
		return options;
	}
	
	public void setOptions(List<Option> options) {
		this.options = options;
	}
	
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public Integer getQuestionnaireId() {
		return questionnaireId;
	}
	
	public void setQuestionnaireId(Integer questionnaireId) {
		this.questionnaireId = questionnaireId;
	}
	
	public String getText() {
		return text;
	}
	
	public void setText(String text) {
		this.text = text;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("question ID: ")
				.append(id)
				.append("\n\"")
				.append(text)
				.append("\"");
		return sb.toString();
	}
	
}
