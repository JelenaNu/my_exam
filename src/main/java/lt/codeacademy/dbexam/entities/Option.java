package lt.codeacademy.dbexam.entities;

import javax.persistence.*;

@Entity
@Table(name = "option")
public class Option {
	
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name = "question_id")
	private Integer questionId;
	
	@Column(name = "text")
	private String text;
	
	@Column(name = "is_answer")
	private boolean isAnswer;
	
	@Column(name = "order_letter")
	private Character orderLetter;
	
	public Option() {
	}
	
	public Option(String text, boolean isAnswer, char orderLetter) {
		this.text = text;
		this.isAnswer = isAnswer;
		this.orderLetter = orderLetter;
	}
	
	public Integer getQuestionId() {
		return questionId;
	}
	
	public void setQuestionId(Integer questionId) {
		this.questionId = questionId;
	}
	
	public Option(Integer questionId, String text, boolean isAnswer, Character orderLetter) {
		this(text, isAnswer, orderLetter);
		this.questionId = questionId;
	}
	
	public Option(String text, boolean isAnswer) {
		this.text = text;
		this.isAnswer = isAnswer;
	}
	
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}

	public String getText() {
		return text;
	}
	
	public void setText(String text) {
		this.text = text;
	}
	
	public boolean isAnswer() {
		return isAnswer;
	}
	
	public void setAnswer(boolean answer) {
		isAnswer = answer;
	}
	
	public Character getOrderLetter() {
		return orderLetter;
	}
	
	public void setOrderLetter(Character orderLetter) {
		this.orderLetter = orderLetter;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(orderLetter)
				.append(") ID: ").append(id)
				.append(", question ID: ").append(questionId)
				.append(" text: \"").append(text)
				.append("\", is right answer: ").append(isAnswer);
		return sb.toString();
	}
}
