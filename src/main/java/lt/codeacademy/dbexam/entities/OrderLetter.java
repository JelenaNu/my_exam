package lt.codeacademy.dbexam.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "statistics_letters")
public class OrderLetter {
	
	@Id
	@Column(name = "order_letter")
	private Character letter;
	
	@Column(name = "count")
	private Integer count;
	
	public OrderLetter() {
	}
	
	public OrderLetter(Character letter, Integer count) {
		this.letter = letter;
		this.count = count;
	}
	
	public Character getLetter() {
		return letter;
	}
	
	public void setLetter(Character letter) {
		this.letter = letter;
	}
	
	public Integer getCount() {
		return count;
	}
	
	public void setCount(Integer count) {
		this.count = count;
	}
	
	@Override
	public String toString() {
		return letter + " count = " + count;
	}
}
