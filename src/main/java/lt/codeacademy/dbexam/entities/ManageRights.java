package lt.codeacademy.dbexam.entities;

import javax.persistence.*;

@Entity
@Table(name = "manage_rights")
@IdClass(ManageRightsPK.class)
public class ManageRights {
	
	@Id
	@Column(name = "login")
	private String login;
	
	@Id
	@Column(name = "questionnaire_id")
	private Integer questionnaireId;
	
	public ManageRights() {
	}
	
	public ManageRights(String login, Integer questionnaireId) {
		this.login = login;
		this.questionnaireId = questionnaireId;
	}
	
	public String getLogin() {
		return login;
	}
	
	public void setLogin(String login) {
		this.login = login;
	}
	
	public Integer getQuestionnaireId() {
		return questionnaireId;
	}
	
	public void setQuestionnaireId(Integer questionnaireId) {
		this.questionnaireId = questionnaireId;
	}
}
