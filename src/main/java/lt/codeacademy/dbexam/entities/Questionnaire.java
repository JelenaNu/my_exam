package lt.codeacademy.dbexam.entities;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "questionnaire")
public class Questionnaire {
	
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name = "theme")
	private String theme;
	
	@Column(name = "description")
	private String description;

	@OneToMany(mappedBy = "questionnaireId", cascade = CascadeType.ALL)
	private List<Question> questions = new ArrayList<>();
	
	public Questionnaire() {
	}
	
	
	public Questionnaire(String theme, String description) {
		this.theme = theme;
		this.description = description;
	}
	
	public List<Question> getQuestions() {
		return questions;
	}
	
	public void setQuestions(List<Question> questions) {
		this.questions = questions;
	}
	
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getTheme() {
		return theme;
	}
	
	public void setTheme(String theme) {
		this.theme = theme;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("ID: ")
				.append(id)
				.append(", THEME: \"")
				.append(theme)
				.append("\"\nDESCRIPTION: ")
				.append(description);
		return sb.toString();
	}
	
	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Questionnaire that = (Questionnaire) o;
		return id.equals(that.id) &&
				theme.equals(that.theme) &&
				description.equals(that.description)/* &&
				questions.equals(that.questions)*/;
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(id, theme, description, questions);
	}
}
