package lt.codeacademy.dbexam.entities;

import javax.persistence.*;

@Entity
@Table(name = "exam")
public class Exam {
	
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name = "questionnaire_id")
	private Integer questionnaireId;
	
	@Column(name = "login")
	private String login;
	
	@Column(name = "right_answers")
	private Integer rightAnswersCount;
	
	public Exam() {
	}
	
	public Exam(Integer questionnaireId, String login, Integer rightAnswersCount) {
		this.questionnaireId = questionnaireId;
		this.login = login;
		this.rightAnswersCount = rightAnswersCount;
	}
	
	public Integer getRightAnswersCount() {
		return rightAnswersCount;
	}
	
	public void setRightAnswersCount(Integer rightAnswersCount) {
		this.rightAnswersCount = rightAnswersCount;
	}
	
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public Integer getQuestionnaireId() {
		return questionnaireId;
	}
	
	public void setQuestionnaireId(Integer questionnaireId) {
		this.questionnaireId = questionnaireId;
	}
	
	public String getLogin() {
		return login;
	}
	
	public void setLogin(String login) {
		this.login = login;
	}
	
	@Override
	public String toString() {
		return "Exam{" +
				"id=" + id +
				", questionnaire_id=" + questionnaireId +
				", login='" + login + '\'' +
				", rightAnswersCount=" + rightAnswersCount +
				'}';
	}
}
