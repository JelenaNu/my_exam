package lt.codeacademy.dbexam;

import lt.codeacademy.dbexam.menu.MainMenu;

import java.util.logging.Level;
import java.util.logging.Logger;

public class App {
	
	public static void main(String[] args) {
		Logger.getLogger("org.hibernate").setLevel(Level.SEVERE);
		new MainMenu().show();
	}
}
