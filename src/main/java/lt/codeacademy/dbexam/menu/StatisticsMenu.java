package lt.codeacademy.dbexam.menu;

import lt.codeacademy.dbexam.entities.OrderLetter;
import lt.codeacademy.dbexam.entities.Questionnaire;
import lt.codeacademy.dbexam.services.ExaminationService;
import lt.codeacademy.dbexam.services.QuestionnaireService;

import java.math.BigInteger;
import java.util.List;

public class StatisticsMenu extends Menu {
	
	private ExaminationService examinationService = new ExaminationService();
	private QuestionnaireService questionnaireService = new QuestionnaireService();
	private final String userLogin;
	
	public StatisticsMenu(String userLogin) {
		this.userLogin = userLogin;
	}
	
	@Override
	protected void show() {
		while (true) {
			System.out.println("Statistics");
			menuHelper.printDelimiter();
			System.out.println("1. Taken exam count");
			System.out.println("2. Right answer count overall");
			System.out.println("3. Right answer count and average count in questionnaires");
			System.out.println("4. Chosen option count by character");
			System.out.println("5. <<< back");
			
			int choice = menuHelper.inputInt();
			switch (choice) {
				case 1:
					showExamCount();break;
				case 2:
					showRightAnswerCount();break;
				case 3:
					showRightAnswerCountAndAverageInQuestionnaires();break;
				case 4:
					showOptionCountByLetter();break;
				case 5:
					return;
				default:
					menuHelper.printWrongInput();
			}
		}
	}
	
	private void showOptionCountByLetter() {
		List<OrderLetter> letters = examinationService.getOrderLetters();
		if (letters.isEmpty()) {
			System.out.println("No options found");
			return;
		}
		letters.forEach(System.out::println);
	}
	
	private void showRightAnswerCountAndAverageInQuestionnaires() {
		List<Questionnaire> questionnaires = questionnaireService.getQuestionnairesByLogin(userLogin);
		if (questionnaires.isEmpty()) {
			System.out.println("No available questionnaires");
			return;
		}
		for (Questionnaire questionnaire : questionnaires){
			BigInteger rightAnswerCount = examinationService.getQuestionnaireRightAnswerCount(questionnaire.getId());
			System.out.println(questionnaire);
			System.out.println("right answer count: " + rightAnswerCount);
			System.out.println("average right answers per exam: " + examinationService.getAverageRightAnswers(questionnaire.getId()));
			System.out.println();
		}
	}
	
	private void showRightAnswerCount() {
		System.out.println("Right answer count total:");
		System.out.println(examinationService.getRightAnswerCount());
	}
	
	private void showExamCount() {
		System.out.println("Exams taken total: ");
		System.out.println(examinationService.getExamCount());
	}
}
