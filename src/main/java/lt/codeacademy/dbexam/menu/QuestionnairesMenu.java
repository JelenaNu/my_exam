package lt.codeacademy.dbexam.menu;

import lt.codeacademy.dbexam.entities.Option;
import lt.codeacademy.dbexam.entities.Question;
import lt.codeacademy.dbexam.entities.Questionnaire;
import lt.codeacademy.dbexam.services.QuestionnaireService;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class QuestionnairesMenu extends Menu{
	
	// every question will have from MIN_OPTION_COUNT to MAX_OPTION_COUNT options to choose
	private final int MAX_OPTION_COUNT = 3;
	private final int MIN_OPTION_COUNT = 3;
	
	private final String userLogin;
	private QuestionnaireService questionnaireService = new QuestionnaireService();
	
	public QuestionnairesMenu(String userLogin) {
		this.userLogin = userLogin;
	}
	
	@Override
	protected void show() {
		while (true) {
			System.out.println("Questionnaires");
			menuHelper.printDelimiter();
			System.out.println("1. Show all available questionnaires");
			System.out.println("2. Add a new questionnaire");
			System.out.println("3. Edit a questionnaire");
			System.out.println("4. Give other user rights to edit");
			System.out.println("5. <<< Back");
			
			int choice = menuHelper.inputInt();
			switch (choice) {
				case 1:
					showQuestionnaires(); break;
				case 2:
					createQuestionnaire(); break;
				case 3:
					showEditQuestionnaire(); break;
				case 4:
					giveUserRightsCommand(); break;
				case 5:
					return;
				default:
					menuHelper.printWrongInput();
			}
		}
	}
	
	private Integer inputQuestionnaireId() {
		System.out.print("Enter a questionnaire ID > ");
		return menuHelper.inputInt();
	}
	
	private Integer inputQuestionId() {
		System.out.print("Enter a question ID > ");
		return menuHelper.inputInt();
	}
	
	private void showEditQuestionnaire() {
		System.out.println("Edit a questionnaire");
		if(showQuestionnaires()) {
			int questionnaireId = inputQuestionnaireId();
			Questionnaire questionnaire = questionnaireService.getQuestionnaire(questionnaireId);
			if (questionnaire != null
					&& questionnaireService.getQuestionnairesByLogin(userLogin).contains(questionnaire)) {
				showQuestionsMenu(questionnaireId);
			} else {
				menuHelper.printWrongInput();
			}
		}
	}
	
	private void giveUserRightsCommand() {
		System.out.println("Give other user rights to edit a questionnaire");
		
		if(showQuestionnaires()) {
			Integer questionnaireId = inputQuestionnaireId();
			System.out.println("Enter a user login > ");
			String login = scanner.nextLine();
			
			if (questionnaireService.giveUserRights(questionnaireId,login)) {
				System.out.println("Successfully gave rights!");
			} else {
				menuHelper.printWrongInput();
			}
		}
	}
	
	private void createQuestionnaire() {
		System.out.println("Create a new questionnaire");
		menuHelper.printDelimiter();
		System.out.print("Enter a theme (max 100 characters) > ");
		String theme = scanner.nextLine();
		System.out.print("Enter a description > ");
		String description = scanner.nextLine();
		
		Questionnaire questionnaire = new Questionnaire(theme, description);
		Integer questionnaireId = questionnaireService.addQuestionnaireAndGetID(userLogin, questionnaire);
		if (questionnaireId != null) {
			System.out.println("Successfully created!");
			addNewQuestionCommand(questionnaireId);
			showQuestionsMenu(questionnaireId);
		} else {
			System.out.println("Couldn't create a questionnaire");
		}
	}
	
	private void showQuestionsMenu(int questionnaireId) {
		while (true) {
			System.out.println("Questions");
			menuHelper.printDelimiter();
			System.out.println("1. Add new question");
			System.out.println("2. Edit a question");
			System.out.println("3. Remove a question");
			System.out.println("4. Show all questions for this questionnaire");
			System.out.println("5. <<< Back");
			System.out.print("> ");
			int choice = menuHelper.inputInt();
			switch (choice) {
				case 1:
					addNewQuestionCommand(questionnaireId); break;
				case 2:
					editQuestionCommand(questionnaireId); break;
				case 3:
					removeQuestionCommand(questionnaireId); break;
				case 4:
					showQuestions(questionnaireId); break;
				case 5:
					return;
				default:
					menuHelper.printWrongInput();
			}
		}
	}
	
	private void editQuestionCommand(int questionnaireId) {
		showQuestions(questionnaireId);
		int questionId = inputQuestionId();
		if (questionnaireService.getQuestion(questionId) != null) {
			showEditQuestionMenu(questionId);
		} else {
			menuHelper.printWrongInput();
		}
	}
	
	private void removeQuestionCommand(int questionnaireId) {
		showQuestions(questionnaireId);
		int questionId = inputQuestionId();
		if (questionnaireService.removeQuestion(questionId)) {
			System.out.println("Successfully deleted!");
		} else {
			System.out.println("Couldn't delete a question");
		}
	}
	
	private void showEditQuestionMenu(int questionId) {
		while (true) {
			System.out.println("Edit question");
			menuHelper.printDelimiter();
			System.out.println("1. Edit question text");
			System.out.println("2. Show options");
			System.out.println("3. Edit options");
			System.out.println("4. <<< Back");
			System.out.print("> ");
			
			int choice = menuHelper.inputInt();
			switch (choice) {
				case 1:
					editQuestionTextCommand(questionId); break;
				case 2:
					showOptions(questionId); break;
				case 3:
					showEditOptionsMenu(questionId); break;
				case 4:
					return;
				default:
					menuHelper.printWrongInput();
			}
		}
	}
	
	private void showEditOptionsMenu(int questionId) {
		while (true) {
			System.out.println("Edit options");
			menuHelper.printDelimiter();
			System.out.println("1. Edit an option");
			System.out.println("2. Add a new option");
			System.out.println("3. Remove an option");
			System.out.println("4. <<< Back");
			System.out.print("> ");
			
			int choice = menuHelper.inputInt();
			switch (choice) {
				case 1:
					editOptionCommand(questionId); break;
				case 2:
					addNewOptionCommand(questionId); break;
				case 3:
					deleteOptionCommand(questionId); break;
				case 4:
					return;
				default:
					menuHelper.printWrongInput();
			}
		}
	}
	private Integer inputOptionId() {
		System.out.println("Enter an option ID > ");
		return menuHelper.inputInt();
	}
	
	private void deleteOptionCommand(int questionId) {
		System.out.println("Remove an option");
		showOptions(questionId);
		int optionId = inputOptionId();
		if (questionnaireService.deleteOption(questionId, optionId)) {
			System.out.println("Successfully deleted!");
		} else {
			System.out.println("Couldn't delete");
		}
	}
	
	private void addNewOptionCommand(int questionId) {
		System.out.println("Add a new option");
		Option option = inputNewOptionData();
		option.setQuestionId(questionId);
		if (questionnaireService.addNewOption(option)) {
			System.out.println("Successfully created an option");
		} else {
			System.out.println("Couldn't create an option");
		}
	}
	
	private void editOptionCommand(int questionId) {
		System.out.println("Edit option");
		showOptions(questionId);
		int optionId = inputOptionId();
		Option option = inputNewOptionData();
		option.setId(optionId);
		option.setQuestionId(questionId);
		if (questionnaireService.editOptionText(option)) {
			System.out.println("Successfully updated an option text");
		} else {
			System.out.println("Couldn't update an option");
		}
	}
	
	private void showOptions(int questionId) {
		System.out.println("Question options:");
		Question question = questionnaireService.getQuestion(questionId);
		if (question != null) {
			System.out.println(questionId);
		}
		List<Option> options = questionnaireService.getOptions(questionId);
		if (!options.isEmpty()) {
			menuHelper.printDelimiter();
			options.sort(Comparator.comparing(Option::getOrderLetter));
			options.forEach(System.out::println);
			menuHelper.printDelimiter();
		}
	}
	
	private void editQuestionTextCommand(int questionId) {
		System.out.println("Enter new text > ");
		String newText = scanner.nextLine();
		if (questionnaireService.editQuestionText(questionId, newText)) {
			System.out.println("Successfully changed a question text");
		} else {
			System.out.println("Couldn't change a text");
		}
	}
	
	private void showQuestions(int questionnaireId) {
		List<Question> questions = questionnaireService.getQuestions(questionnaireId);
		if (questions.isEmpty()) {
			System.out.println("This questionnaire has no questions");
			return;
		}
		System.out.println("Show questionnaire (id = " + questionnaireId + ") questions: ");
		menuHelper.printDelimiter();
		for (int i = 0; i < questions.size(); i++) {
			System.out.print(i + 1);
			System.out.print(". ");
			System.out.println(questions.get(i));
		}
		menuHelper.printDelimiter();
	}
	
	private void addNewQuestionCommand(Integer questionnaireId) {
		System.out.println("Add a new question");
		menuHelper.printDelimiter();
		System.out.print("Question text > ");
		String questionText = scanner.nextLine();
		
		int optionCount = 0;
		while (true) {
			System.out.print("Enter an option count > ");
			optionCount = menuHelper.inputInt();
			if (optionCount <= MAX_OPTION_COUNT &&  optionCount >= MIN_OPTION_COUNT) {
				break;
			}
		}
		
		List<Option> options = new ArrayList<>();
		for (int i = 0; i < optionCount; i++) {
			options.add(inputNewOptionData());
		}
		if (questionnaireService.createNewQuestion(questionnaireId, questionText, options)) {
			System.out.println("Successfully created a question");
		} else {
			System.out.println("Couldn't create a question");
		}
	}
	private Option inputNewOptionData() {
		System.out.print("Enter an option text > ");
		String optionText = scanner.nextLine();
		
		System.out.println("Is this the question answer?");
		System.out.println("0. no");
		System.out.println("1. yes");
		System.out.print(" > ");
		boolean isAnswer = false;
		while (true) {
			int choice = menuHelper.inputInt();
			if (choice == 1) {
				isAnswer = true;
				break;
			}
			if (choice == 0) {
				break;
			}
		}
		return new Option(optionText, isAnswer);
	}
	
	private boolean showQuestionnaires() {
		System.out.println("Show available questionnaires");
		List questionnaires = questionnaireService.getQuestionnairesByLogin(userLogin);
		if (questionnaires != null && !questionnaires.isEmpty()) {
			menuHelper.printDelimiter();
			for (int i = 0; i < questionnaires.size(); i++) {
				System.out.print(i + 1);
				System.out.print(". ");
				System.out.println(questionnaires.get(i));
			}
			menuHelper.printDelimiter();
			return true;
		} else {
			System.out.println("No available questionnaires");
			return false;
		}
	}
}
