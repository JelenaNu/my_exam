package lt.codeacademy.dbexam.menu;

import lt.codeacademy.dbexam.entities.Exam;
import lt.codeacademy.dbexam.entities.Option;
import lt.codeacademy.dbexam.entities.Question;
import lt.codeacademy.dbexam.entities.Questionnaire;
import lt.codeacademy.dbexam.services.ExaminationService;
import lt.codeacademy.dbexam.services.QuestionnaireService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ExamMenu extends Menu {
	
	private ExaminationService examinationService = new ExaminationService();
	private QuestionnaireService questionnaireService = new QuestionnaireService();
	private final String userLogin;
	
	public ExamMenu(String userLogin) {
		this.userLogin = userLogin;
	}
	
	@Override
	public void show() {
		while (true) {
			System.out.println("Exams");
			menuHelper.printDelimiter();
			System.out.println("1. Take exam by questionnaire id");
			System.out.println("2. Search questionnaire by theme");
			System.out.println("3. Show all questionnaires");
			System.out.println("4. <<< Back");
			System.out.print("> ");
			
			int choice = menuHelper.inputInt();
			switch (choice) {
				case 1:
					takeExamCommand();break;
				case 2:
					showQuestionnairesByTheme();break;
				case 3:
					showAllQuestionnaires();break;
				case 4:
					return;
				default:
					menuHelper.printWrongInput();
			}
		}
	}
	
	private void takeExamCommand() {
		System.out.println("Taking an exam");
		menuHelper.printDelimiter();
		showAllQuestionnaires();
		
		System.out.println("Enter a questionnaire ID > ");
		int questionnaireId = menuHelper.inputInt();
		List<Question> questions = questionnaireService.prepareExamQuestions(questionnaireId);
		if (!questions.isEmpty()) {
			takeExam(questions);
		} else {
			System.out.println("Couldn't prepare questions");
		}
	}
	
	private void takeExam(List<Question> questions) {
		int rightAnswerCount = 0;
		Map<Character,Integer> letterCountsMap = new HashMap<>();
		
		for (int i = 0; i < questions.size(); i++) {
			System.out.println(i + 1 + ". " + questions.get(i).getText());
			for (int j = 0; j < questions.get(i).getOptions().size(); j++) {
				Option option = questions.get(i).getOptions().get(j);
				System.out.println(j + 1 + ". " + option.getOrderLetter()+ ") " + option.getText());
			}
			
			System.out.print("Enter answer number > ");
			while (true) {
				int answerNumber = menuHelper.inputInt();
				if (answerNumber > 0 && answerNumber <= questions.get(i).getOptions().size()) {
					Option option = questions.get(i).getOptions().get(answerNumber - 1);
					if (option.isAnswer()) {
						rightAnswerCount++;
					}
					Character orderLetter = option.getOrderLetter();
					if (containsLetter(letterCountsMap, orderLetter)) {
						for (Map.Entry<Character,Integer> entry : letterCountsMap.entrySet()) {
							if (entry.getKey().equals(orderLetter)) {
								entry.setValue(entry.getValue() + 1);
								break;
							}
						}
					} else {
						letterCountsMap.put(orderLetter, 1);
					}
					break;
				}
			}
		}
		printResult(rightAnswerCount, questions.size());
		Exam exam = new Exam(questions.get(0).getQuestionnaireId(), userLogin, rightAnswerCount);
		if (!examinationService.updateStatistics(exam, letterCountsMap)) {
			System.out.println("Couldn't save your results");
		}
	}
	
	private void printResult(int rightAnswerCount, int questionCount){
		menuHelper.printDelimiter();
		System.out.println("Exam is finished!");
		int percent = 0;
		try {
			percent = 100 * rightAnswerCount / questionCount;
		} catch (ArithmeticException ignored) {
			// divide by zero
		}
		System.out.println("Your result is: " + percent + "%");
		System.out.println("Right answers: " + rightAnswerCount + " of " + questionCount + " questions");
		menuHelper.printDelimiter();
	}
	
	private void showQuestionnairesByTheme() {
		System.out.println("Enter a word to search by >");
		String keyWord = scanner.nextLine();
		List<Questionnaire> questionnaires = questionnaireService.getQuestionnairesByTheme(keyWord);
		menuHelper.printDelimiter();
		if (questionnaires.isEmpty()) {
			System.out.println("No questionnaires found");
		}
		printList(questionnaires);
		menuHelper.printDelimiter();
	}
	
	private void showAllQuestionnaires() {
		List<Questionnaire> questionnaires = questionnaireService.getAllQuestionnaires();
		menuHelper.printDelimiter();
		if (questionnaires.isEmpty()) {
			System.out.println("No questionnaires found");
		}
		printList(questionnaires);
		menuHelper.printDelimiter();
	}
	
	private void printList(List<?> list) {
		for (int i = 0; i < list.size(); i++) {
			System.out.print(i + 1);
			System.out.print(". ");
			System.out.println(list.get(i));
		}
	}
	
	private boolean containsLetter(Map<Character, Integer> letterCountsMap, Character orderLetter) {
		for (Map.Entry<Character,Integer> entry : letterCountsMap.entrySet()) {
			if (entry.getKey().equals(orderLetter)) {
				return true;
			}
		}
		return false;
	}
}
