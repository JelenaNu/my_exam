package lt.codeacademy.dbexam.menu;

import java.util.Scanner;

public class MenuHelper {
	
	private Scanner scanner = new Scanner(System.in);
	
	protected void printDelimiter() {
		System.out.println("-----------------------------------------------------");
	}
	
	protected int inputInt() {
		try {
			return Integer.parseInt(scanner.nextLine());
		} catch (NumberFormatException e) {
			return 0;
		}
	}
	
	protected void printWrongInput() {
		System.out.println("Wrong input. Try again.");
	}
}
