package lt.codeacademy.dbexam.menu;

import lt.codeacademy.dbexam.services.AccountService;

public class MainMenu extends Menu {
	
	private AccountService accountService = new AccountService();
	
	@Override
	public void show() {
		String userLogin = null;
		
		while (userLogin == null) {
			System.out.println("1. Sign in");
			System.out.println("2. Register a new account");
			System.out.println("3. Exit");
			System.out.print("> ");
			
			int choice = menuHelper.inputInt();
			switch (choice) {
				case 1:
					userLogin = signInCommand(); break;
				case 2:
					userLogin = registerCommand();break;
				case 3:
					System.exit(0);
				default:
					menuHelper.printWrongInput();
			}
		}
		showMain(userLogin);
	}
	
	private String registerCommand() {
		System.out.println("register a new user");
		menuHelper.printDelimiter();
		
		String login = inputLogin();
		if (!accountService.isLoginAvailable(login)) {
			menuHelper.printWrongInput();
			return null;
		}
		
		String password = inputPassword();
		System.out.print("Enter your name > ");
		String name = scanner.nextLine();
		System.out.print("Enter your last name > ");
		String lastName = scanner.nextLine();
		
		if (accountService.registerAccount(login, password, name, lastName)) {
			System.out.println("Successfully registered!");
			return login;
		}
		return null;
	}
	
	private String signInCommand() {
		System.out.println("Sign in");
		menuHelper.printDelimiter();
		
		String login = inputLogin();
		String password = inputPassword();
		if (accountService.signIn(login, password)) {
			System.out.println("Successfully entered into account");
			return login;
		}
		System.out.println("Couldn't sign in. Try again.");
		return null;
	}
	
	private String inputLogin() {
		System.out.print("Enter a login > ");
		return scanner.nextLine();
	}
	
	private String inputPassword() {
		System.out.print("Enter a password > ");
		return scanner.nextLine();
	}
	
	private void showMain(String userLogin) {
		while (true) {
			System.out.println("Main menu");
			menuHelper.printDelimiter();
			System.out.println("1. Manage questionnaires");
			System.out.println("2. Take exams");
			System.out.println("3. Statistics");
			System.out.println("4. Exit");
			System.out.print("> ");
			
			int choice = menuHelper.inputInt();
			switch (choice) {
				case 1:
					new QuestionnairesMenu(userLogin).show(); break;
				case 2:
					new ExamMenu(userLogin).show(); break;
				case 3:
					new StatisticsMenu(userLogin).show(); break;
				case 4:
					System.exit(0);
				default:
					menuHelper.printWrongInput();
			}
		}
	}
}

