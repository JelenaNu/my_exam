package lt.codeacademy.dbexam.menu;

import java.util.Scanner;

public abstract class Menu {
	
	protected Scanner scanner = new Scanner(System.in);
	protected MenuHelper menuHelper = new MenuHelper();
	
	abstract protected void show();
}
