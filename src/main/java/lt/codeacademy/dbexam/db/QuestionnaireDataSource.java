package lt.codeacademy.dbexam.db;

import lt.codeacademy.dbexam.entities.*;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.ArrayList;
import java.util.List;

public class QuestionnaireDataSource extends DataSource {
	
	public List selectQuestionnairesByLogin(String login) {
		List questionnaires = null;
		Session session = createSession();
		try {
			Query query = session.createQuery(
					"select q from Questionnaire q " +
								"inner join ManageRights m on q.id = m.questionnaireId " +
								"where login = :login");
			
			query.setParameter("login", login);
			questionnaires = query.list();
		} catch (Exception e) {
			//e.printStackTrace();
		} finally {
		session.close();
		}
		return questionnaires;
	}
	
	public Integer insertNewQuestionnaireAndRights(Questionnaire questionnaire, String login) {
		Session session = createSession();
		Integer id = null;
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			session.save(questionnaire);
			id = questionnaire.getId();
			session.save(new ManageRights(login, questionnaire.getId()));
			tx.commit();
		} catch (Exception e) {
			if (tx != null) {
				tx.rollback();
			}
			//e.printStackTrace();
		} finally {
			session.close();
		}
		return id;
	}
	
	public boolean insertNewQuestionAndOptions(Question question, List<Option> options) {
		Session session = createSession();
		Transaction tx = null;
		boolean isAdded = false;
		try {
			tx = session.beginTransaction();
			session.save(question);
			int questionId = question.getId();
			for (Option option: options) {
				option.setQuestionId(questionId);
				session.save(option);
			}
			tx.commit();
			isAdded = true;
		} catch (Exception e) {
			if (tx != null) {
				tx.rollback();
			}
			//e.printStackTrace();
		} finally {
			session.close();
		}
		return isAdded;
	}
	
	public List<Question> selectQuestions(int questionnaireId) {
		List<Question> questions = null;
		Session session = createSession();
		try {
			Query query = session.createQuery(
					"from Question where questionnaireId = :questionnaireId");
			
			query.setParameter("questionnaireId", questionnaireId);
			questions = query.list();
		} catch (Exception e) {
			//e.printStackTrace();
		} finally {
			session.close();
		}
		return questions;
	}
	
	public Question selectQuestion(int questionId) {
		Session session = createSession();
		Question question = null;
		try {
			Query<Question> query = session.createQuery("from Question where id = :questionId", Question.class);
			query.setParameter("questionId", questionId);
			List<Question> questions = query.list();
			if (!questions.isEmpty()) {
				question = questions.get(0);
			}
		} catch (Exception e) {
			//e.printStackTrace();
		} finally {
			session.close();
		}
		return question;
	}
	
	public boolean updateQuestion(int questionId, String newText) {
		Session session = createSession();
		Transaction tx = null;
		boolean isUpdated = false;
		try {
			tx = session.beginTransaction();
			Query<Question> query = session.createQuery("update Question set text = :newText " +
					"where id = :questionId");
			query.setParameter("newText", newText);
			query.setParameter("questionId", questionId);
			query.executeUpdate();
			tx.commit();
			isUpdated = true;
		} catch (Exception e) {
			if (tx != null) {
				tx.rollback();
			}
			//e.printStackTrace();
		} finally {
			session.close();
		}
		return isUpdated;
	}
	
	public Questionnaire selectQuestionnaire(int questionnaireId) {
		Session session = createSession();
		Questionnaire questionnaire = null;
		try {
			Query<Questionnaire> query = session.createQuery("from Questionnaire " +
					"where id = :questionnaireId", Questionnaire.class);
			query.setParameter("questionnaireId", questionnaireId);
			List<Questionnaire> questionnaires = query.list();
			if (!questionnaires.isEmpty()) {
				questionnaire = questionnaires.get(0);
			}
		} catch (Exception e) {
			//e.printStackTrace();
		} finally {
			session.close();
		}
		return questionnaire;
	}
	
	public boolean insertQuestionnaireRights(ManageRights manageRights) {
		boolean isAdded = false;
		Session session = createSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			session.save(manageRights);
			tx.commit();
			isAdded = true;
		} catch (Exception e) {
			if (tx != null) {
				tx.rollback();
			}
			//e.printStackTrace();
		} finally {
			session.close();
		}
		return isAdded;
	}
	
	public List<Option> selectOptions(int questionId) {
		List<Option> options = new ArrayList<>();
		Session session = createSession();
		try {
			Query<Option> query = session.createQuery(
					"from Option where questionId = :questionId", Option.class);
			
			query.setParameter("questionId", questionId);
			options = query.list();
		} catch (Exception e) {
			//e.printStackTrace();
		} finally {
			session.close();
		}
		return options;
	}
	
	public boolean updateOption(Option option) {
		Session session = createSession();
		Transaction tx = null;
		boolean isUpdated = false;
		try {
			tx = session.beginTransaction();
			Query<Option> query = session.createQuery("update Option set text = :newText, isAnswer = :isAnswer " +
					"where id = :optionId");
			query.setParameter("newText", option.getText());
			query.setParameter("isAnswer", option.isAnswer());
			query.setParameter("optionId", option.getId());
			query.executeUpdate();
			tx.commit();
			isUpdated = true;
		} catch (Exception e) {
			if (tx != null) {
				tx.rollback();
			}
			//e.printStackTrace();
		} finally {
			session.close();
		}
		return isUpdated;
	}
	
	public boolean insertNewOption(Option option) {
		boolean isAdded = false;
		Session session = createSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			session.save(option);
			tx.commit();
			isAdded = true;
		} catch (Exception e) {
			if (tx != null) {
				tx.rollback();
			}
			//e.printStackTrace();
		} finally {
			session.close();
		}
		return isAdded;
	}
	
	public boolean deleteOption(Option optionToDelete, List<Option> optionsToUpdate) {
		Session session = createSession();
		Transaction tx = null;
		boolean isDeleted = false;
		try {
			tx = session.beginTransaction();
			session.delete(optionToDelete);
			Query<Option> query;
			for (Option option : optionsToUpdate) {
				query = session.createQuery("update Option set orderLetter = :orderLetter " +
						"where id = :optionId");
				query.setParameter("orderLetter", option.getOrderLetter());
				query.setParameter("optionId", option.getId());
				query.executeUpdate();
			}
			tx.commit();
			isDeleted = true;
		} catch (Exception e) {
			if (tx != null) {
				tx.rollback();
			}
			//e.printStackTrace();
		} finally {
			session.close();
		}
		return isDeleted;
	}
	
	public List<Questionnaire> selectAllQuestionnaires() {
		Session session = createSession();
		Query<Questionnaire> query = session.createQuery("from Questionnaire", Questionnaire.class);
		List<Questionnaire> questionnaires = query.list();
		session.close();
		return questionnaires;
	}
	
	public List<Questionnaire> selectQuestionnairesByTheme(String keyWord) {
		Session session = createSession();
		Query<Questionnaire> query = session.createQuery(
				"from Questionnaire where theme like :keyWord", Questionnaire.class);
		query.setParameter("keyWord", "%" + keyWord + "%");
		List<Questionnaire> questionnaires = query.list();
		session.close();
		return questionnaires;
	}
	
	public boolean deleteQuestion(int questionId) {
		Question question = selectQuestion(questionId);
		Session session = createSession();
		boolean isDeleted = false;
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			session.delete(question);
			tx.commit();
			isDeleted = true;
		} catch (Exception e) {
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			session.close();
		}
		return isDeleted;
	}
}
