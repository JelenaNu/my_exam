package lt.codeacademy.dbexam.db;

import lt.codeacademy.dbexam.entities.Exam;
import lt.codeacademy.dbexam.entities.OrderLetter;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.math.BigInteger;
import java.util.List;

public class ExaminationDataSource extends DataSource{
	
	public List<OrderLetter> selectOrderLetters() {
		Session session = createSession();
		Query query = session.createQuery("from OrderLetter", OrderLetter.class);
		List<OrderLetter> orderLetters = query.list();
		session.close();
		return orderLetters;
	}
	
	public boolean updateStatistics(Exam exam, List<OrderLetter> orderLetters) {
		boolean isAdded = false;
		Session session = createSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			session.save(exam);
			for (OrderLetter orderLetter : orderLetters) {
				session.saveOrUpdate(orderLetter);
			}
			tx.commit();
			isAdded = true;
		} catch (Exception e) {
			if (tx != null) {
				tx.rollback();
			}
			//e.printStackTrace();
		} finally {
			session.close();
		}
		return isAdded;
	}
	
	public BigInteger selectExamCount() {
		Session session = createSession();
		Query query = session.createNativeQuery("SELECT COUNT(e.id) FROM Exam e");
		BigInteger count = (BigInteger) query.getSingleResult();
		session.close();
		return count;
	}
	
	public BigInteger selectRightAnswerCount() {
		Session session = createSession();
		Query query = session.createNativeQuery("SELECT SUM(e.right_answers) FROM Exam e");
		BigInteger count = (BigInteger) query.getSingleResult();
		session.close();
		return count;
	}
	
	public BigInteger selectQuestionnaireRightAnswerCount(int questionnaireId) {
		Session session = createSession();
		Query query = session.createNativeQuery(
				"SELECT SUM(e.right_answers) FROM Exam e " +
				"WHERE e.questionnaire_id = :id");
		query.setParameter("id", questionnaireId);
		BigInteger count = (BigInteger) query.getSingleResult();
		session.close();
		return count;
	}
	
	public BigInteger selectExamCountByQuestionnaire(int questionnaireId) {
		Session session = createSession();
		Query query = session.createNativeQuery("SELECT COUNT(e.id) FROM Exam e " +
				"WHERE e.questionnaire_id = :id");
		query.setParameter("id", questionnaireId);
		BigInteger count = (BigInteger) query.getSingleResult();
		session.close();
		return count;
	}
}
