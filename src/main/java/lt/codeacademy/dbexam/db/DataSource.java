package lt.codeacademy.dbexam.db;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public abstract class DataSource {
	
	protected Session createSession() {
		SessionFactory factory = new Configuration()
				.configure()
				.buildSessionFactory();
		return factory.openSession();
	}
}
