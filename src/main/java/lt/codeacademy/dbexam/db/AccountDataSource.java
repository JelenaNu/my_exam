package lt.codeacademy.dbexam.db;

import lt.codeacademy.dbexam.entities.Account;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.List;

public class AccountDataSource extends DataSource{
	
	public boolean insertNewAccount(Account account) {
		boolean isAdded = false;
		Session session = createSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			session.save(account);
			tx.commit();
			isAdded = true;
		} catch (Exception e) {
			if (tx != null) {
				tx.rollback();
			}
			//e.printStackTrace();
		} finally {
			session.close();
		}
		return isAdded;
	}
	
	public Account selectAccount(String login) {
		Session session = createSession();
		Account account = null;
		try {
			Query<Account> query = session.createQuery("from Account where login = :login", Account.class);
			query.setParameter("login", login);
			List<Account> accounts = query.list();
			if (!accounts.isEmpty()) {
				account = accounts.get(0);
			}
		} catch (Exception e) {
			//e.printStackTrace();
		} finally {
			session.close();
		}
		return account;
	}
}
