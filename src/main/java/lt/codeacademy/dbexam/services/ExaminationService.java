package lt.codeacademy.dbexam.services;

import lt.codeacademy.dbexam.db.ExaminationDataSource;
import lt.codeacademy.dbexam.entities.Exam;
import lt.codeacademy.dbexam.entities.OrderLetter;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ExaminationService {
	
	private ExaminationDataSource dataSource = new ExaminationDataSource();
	
	public boolean updateStatistics(Exam exam, Map<Character, Integer> letterCountsMap) {
		List<OrderLetter> orderLetters = dataSource.selectOrderLetters();
		for (Map.Entry<Character,Integer> entry : letterCountsMap.entrySet()) {
			if (!contains(orderLetters, entry.getKey())) {
				orderLetters.add(new OrderLetter(entry.getKey(), entry.getValue()));
				continue;
			}
			for (OrderLetter sol : orderLetters) {
				if (sol.getLetter().equals(entry.getKey())) {
					sol.setCount(sol.getCount() + entry.getValue());
				}
			}
		}
		return dataSource.updateStatistics(exam, orderLetters);
	}
	
	private boolean contains(List<OrderLetter> letters, Character letter) {
		for (OrderLetter orderLetter : letters) {
			if (orderLetter.getLetter().equals(letter)) {
				return true;
			}
		}
		return false;
	}
	
	public BigInteger getExamCount() {
		return dataSource.selectExamCount();
	}
	
	public BigInteger getRightAnswerCount() {
		return dataSource.selectRightAnswerCount();
	}
	
	public BigInteger getQuestionnaireRightAnswerCount(int questionnaireId) {
		BigInteger rightAnswerCount = dataSource.selectQuestionnaireRightAnswerCount(questionnaireId);
		if (rightAnswerCount == null) {
			rightAnswerCount = BigInteger.valueOf(0);
		}
		return rightAnswerCount;
	}
	
	public BigInteger getAverageRightAnswers(Integer questionnaireId){
		BigInteger rightAnswerCount = getQuestionnaireRightAnswerCount(questionnaireId);
		BigInteger examCount = getExamCountByQuestionnaire(questionnaireId);
		
		if (examCount.equals(BigInteger.valueOf(0))) {
			return BigInteger.valueOf(0);
		}
		return rightAnswerCount.divide(examCount);
	}
	
	public BigInteger getExamCountByQuestionnaire(int questionnaireId) {
		return dataSource.selectExamCountByQuestionnaire(questionnaireId);
	}
	
	public List<OrderLetter> getOrderLetters() {
		List<OrderLetter> letters = dataSource.selectOrderLetters();
		if (letters == null) {
			return new ArrayList<>();
		}
		return letters;
	}
}
