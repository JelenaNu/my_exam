package lt.codeacademy.dbexam.services;

import lt.codeacademy.dbexam.db.QuestionnaireDataSource;
import lt.codeacademy.dbexam.entities.*;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class QuestionnaireService {
	
	private final QuestionnaireDataSource dataSource= new QuestionnaireDataSource();
	
	public Integer addQuestionnaireAndGetID(String login, Questionnaire questionnaire) {
		return dataSource.insertNewQuestionnaireAndRights(questionnaire, login);
	}
	
	public List getQuestionnairesByLogin(String login) {
		return dataSource.selectQuestionnairesByLogin(login);
	}
	
	public boolean createNewQuestion(Integer questionnaireId, String questionText, List<Option> optionList) {
		char orderLetter = 'a';
		List<Option> options = new ArrayList<>();
		for (Option option : optionList) {
			option.setOrderLetter(orderLetter++);
			options.add(option);
		}
		return dataSource.insertNewQuestionAndOptions(new Question(questionnaireId, questionText), options);
	}
	
	public List<Question> getQuestions(int questionnaireId) {
		List<Question> questions = dataSource.selectQuestions(questionnaireId);
		if (questions == null) {
			return new ArrayList<>();
		}
		return questions;
	}
	
	public Question getQuestion(int questionId) {
		return dataSource.selectQuestion(questionId);
	}
	
	public boolean editQuestionText(int questionId, String newText) {
		return dataSource.updateQuestion(questionId, newText);
	}
	
	public Questionnaire getQuestionnaire(int questionnaireId) {
		return dataSource.selectQuestionnaire(questionnaireId);
	}
	
	public boolean giveUserRights(int questionnaireId, String login) {
		Questionnaire questionnaire = getQuestionnaire(questionnaireId);
		if (questionnaire != null && !new AccountService().isLoginAvailable(login)) {
			return dataSource.insertQuestionnaireRights(new ManageRights(login, questionnaire.getId()));
		}
		return false;
	}
	
	public List<Option> getOptions(int questionId) {
		return dataSource.selectOptions(questionId);
	}
	
	public boolean editOptionText(Option option) {
		return dataSource.updateOption(option);
	}
	
	public boolean addNewOption(Option option) {
		List<Option> options = getOptions(option.getQuestionId());
		options.sort(Comparator.comparing(Option::getOrderLetter));
		char orderLetter = 'a';
		for (Option o : options) {
			if (o.getOrderLetter() != orderLetter) {
				break;
			}
			orderLetter++;
		}
		option.setOrderLetter(orderLetter);
		return dataSource.insertNewOption(option);
	}
	
	public boolean deleteOption(int questionId, int optionId) {
		List<Option> options = dataSource.selectOptions(questionId);
		options.sort(Comparator.comparing(Option::getOrderLetter));
		Character orderLetter = null;
		Option optionToDelete = null;
		for (Option option : options) {
			if (option.getId() == optionId) {
				orderLetter = option.getOrderLetter();
				optionToDelete = option;
				continue;
			}
			if (orderLetter != null) {
				option.setOrderLetter(orderLetter);
				orderLetter ++;
			}
		}
		options.remove(optionToDelete);
		return dataSource.deleteOption(optionToDelete, options);
	}
	
	public List<Questionnaire> getAllQuestionnaires() {
		List<Questionnaire> questionnaires = dataSource.selectAllQuestionnaires();
		if (questionnaires == null) {
			questionnaires = new ArrayList<>();
		}
		return questionnaires;
	}
	
	public List<Questionnaire> getQuestionnairesByTheme(String keyWord) {
		List<Questionnaire> questionnaires = dataSource.selectQuestionnairesByTheme(keyWord);
		if (questionnaires == null) {
			questionnaires = new ArrayList<>();
		}
		return questionnaires;
	}
	
	public List<Question> prepareExamQuestions(int questionnaireId) {
		List<Question> questions = getQuestions(questionnaireId);
		if (!questions.isEmpty()) {
			for (Question question : questions) {
				List<Option> options = getOptions(question.getId());
				if (!options.isEmpty()){
					question.setOptions(options);
					options.sort(Comparator.comparing(Option::getOrderLetter));
				}
			}
		}
		return questions;
	}
	
	public boolean removeQuestion(int questionId) {
		return dataSource.deleteQuestion(questionId);
	}
}
