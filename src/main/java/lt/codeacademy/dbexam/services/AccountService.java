package lt.codeacademy.dbexam.services;

import lt.codeacademy.dbexam.db.AccountDataSource;
import lt.codeacademy.dbexam.entities.Account;
import org.apache.commons.codec.digest.DigestUtils;

public class AccountService {
	private AccountDataSource dataSource;
	
	public AccountService() {
		dataSource = new AccountDataSource();
	}
	
	public boolean signIn(String login, String password) {
		if (login.isEmpty()) {
			return false;
		}
		Account account = dataSource.selectAccount(login);
		return account != null && account.getPassword().equals(DigestUtils.md5Hex(password));
	}
	
	public boolean registerAccount(String login, String password, String name, String lastName) {
		return dataSource.insertNewAccount(new Account(login, DigestUtils.md5Hex(password), name, lastName));
	}
	
	public boolean isLoginAvailable(String login) {
		if (login.isEmpty()) {
			return false;
		}
		return dataSource.selectAccount(login) == null;
	}
}
